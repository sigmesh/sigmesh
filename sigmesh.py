#!/usr/bin/env python
#
#  Usage:
#
#  Prepare a list of signatures:
#
#      gpg --with-colons --check-sigs --fingerprint > sigs.txt
#
#  Run this:
#
#      python sigmesh.py 01234567 76543210,FEDCBA90 < sigs.txt > graph.dot
#
#  Run dot on the result:
#
#      dot -Tgif -o graph.gif graph.dot

import sys

class Badfile(Exception): pass

GPG_TYPE = 0
GPG_CALC_TRUST = 1
GPG_KEYSIZE = 2
GPG_ALGO = 3
GPG_KEY_ID = 4
GPG_CREATED = 5
GPG_EXPIRY = 6
GPG_LOCAL_ID = 7
GPG_OWNER_TRUST = 8
GPG_USER_ID = 9
GPG_SIGNATURE_CLASS = 10
GPG_KEY_CAPABILITIES = 11

class graph:
    def __init__(self, keyring, trusted, target):
        self.__k = keyring
        self.__trusted = trusted
        self.__target = target

        self.__nodes = {}

    def solve(self, forbidden):
        self.__k.reset()
        for f in forbidden:
            self.__k.mark_forbidden(f)
        nodes = self.__k.find_mesh(self.__trusted, self.__target)
        if len(nodes):
            for n in nodes:
                self.__nodes[n] = None
            return 1
        else:
            return 0

    def potential_step(self, multiplier):
        flux = 0
        for node in self.__nodes.keys():
            flux += self.__k.node(node).potential_step(multiplier)
        return flux

    def compute_potentials(self):
        self.__k.reset()
        self.__k.init_potentials(self.__nodes)
        self.__k.node32(self.__target).set_potential(0)
        self.__k.node32(self.__trusted).set_potential(1)
        self.run_graph()
        self.prune_idle_nodes()
        self.prune_small_arcs()
        self.run_graph()
        self.merge_isomorphic_nodes()

    def merge_isomorphic_nodes(self):
        for node in self.__nodes.keys():
            if self.__k.node(node).merge_isomorphic_node():
                del self.__nodes[node]

    def run_graph(self):
        step = 1
        oldflux = None
        badflux = 2
        factor = 0.4
        while 1:
            flux = self.potential_step(factor)
            if flux < 0.000001:
                break
            if oldflux != None and flux > oldflux:
                if badflux:
                    badflux -= 1
                else:
                    factor = 0
            oldflux = flux
            step += 1

    def prune_idle_nodes(self):
        for node in self.__nodes.keys():
            self.__k.node(node).compute_current()
            if self.__k.node(node).current() == 0:
                sys.stderr.write("Removing %s due to low current\n" % name)
                del self.__nodes[name]

    def prune_small_arcs(self):
        for node in self.__nodes.keys():
            self.__k.node(node).prune_small_arcs()

    def emit(self):
        self.compute_potentials()

        print "digraph \"ceder->tege\" {"
        print "node [style=filled]"
        res = []
        for n in self.__nodes.keys():
            res.append((self.__k.node(n).potential(), n))
        res.sort()
        for p, n in res:
            self.__k.node(n).printnode()
        for p, n in res:
            self.__k.node(n).printarcs()
        print "}"

    

class person:
    __color = "white"
    __marked = 0

    def __init__(self, pub_fields):
        assert pub_fields[GPG_TYPE] == "pub"
        self.__good = pub_fields[GPG_CALC_TRUST] in ["q", "m", "f", "u", "-"]
        self.__key_id = pub_fields[GPG_KEY_ID]
        self.__created = pub_fields[GPG_CREATED]
        self.__ownertrust = pub_fields[GPG_OWNER_TRUST]
        self.__uids = [pub_fields[GPG_USER_ID]]
        self.__sigs = []
        self.__potential = 0.5
        self.__forw = []
        self.__back = []
        self.__weight = 1
        self.reset()

        self.__label = "%s\\n%s" % (
            self.__uids[0].replace(" <", "\\n<", 1),
            self.__key_id[-8:])

    def reset(self):
        self.__visited = 0
        self.__rsigs = []
        self.__rvisited = 0
        self.__forbidden = 0

    def init_potential(self, nodes):
        global ring

        self.__fixed = 0
        for s in self.__sigs:
            if nodes.has_key(s):
                other = ring.node(s)
                self.__forw.append(other)
                other.add_back(self)

    def add_back(self, other):
        self.__back.append(other)

    def set_potential(self, pot):
        self.__potential = pot
        self.__fixed = 1

    def potential(self):
        return self.__potential

    def potential_step(self, multiplier):
        if self.__fixed:
            return 0
        old_p = self.__potential
        while 1:
            s = 0
            n = 0
            for node in self.__forw:
                if node.potential() >= old_p:
                    s += node.potential()
                    n += 1
            for node in self.__back:
                if node.potential() <= old_p:
                    s += node.potential()
                    n += 1
            p = s / n
            if abs(p - old_p) < 0.0000001:
                break
            old_p = p
        diff = p - self.__potential
        self.__potential = p + multiplier * diff
        return abs(diff)

    def compute_current(self):
        cin = 0
        for node in self.__forw:
            if node.potential() >= self.__potential:
                cin += node.potential() - self.potential()
        cout = 0
        for node in self.__back:
            if node.potential() < self.__potential:
                cout += self.potential() - node.potential()
        if self.__marked:
            self.__current = max(cin, cout)
        else:
            self.__current = min(cin, cout)

    def current(self):
        return self.__current * self.__weight

    def mark_forbidden(self):
        self.__forbidden = 1

    def set_uid(self, uid_fields):
        assert uid_fields[GPG_TYPE] == "uid"
        self.__uids.append(uid_fields[GPG_USER_ID])

    def set_sig(self, sig_fields):
        assert sig_fields[GPG_TYPE] == "sig"
        if sig_fields[GPG_CALC_TRUST] != "!":
            return
        if sig_fields[GPG_KEY_ID] in self.__sigs:
            return
        if sig_fields[GPG_KEY_ID] == self.__key_id:
            return
        self.__sigs.append(sig_fields[GPG_KEY_ID])

    def set_fpr(self, fpr_fields):
        return

    def id(self):
        return self.__key_id

    def prunesigs(self):
        global ring

        newsigs = []
        for s in self.__sigs:
            if ring.has_key(s):
                newsigs.append(s)
        self.__sigs = newsigs

    def mark(self):
        self.__color = "yellow"
        self.__marked = 1

    def visit(self, parent, level):
        if not self.__good:
            return []
        if self.__forbidden:
            return []
        if parent is not None:
            if parent not in self.__rsigs:
                if level <= self.__visited or not self.__visited:
                    # print "rsig", self.id(), parent
                    self.__rsigs.append(parent)
        if self.__visited:
            return []
        else:
            self.__visited = level
            res = []
            for node in self.__sigs:
                res.append((self.id(), node))
            return res

    def rvisit(self, all_nodes, child):
        if not self.__rvisited:
            all_nodes.append(self.id())
        if self.__rvisited:
            return []
        else:
            self.__rvisited = 1
            res = []
            for node in self.__rsigs:
                res.append((self.id(), node))
            return res

    def printnode(self):
        if self.__weight > 1:
            separator = "\\n\\n"
        else:
            separator = "\\n"
        print "\"%s\" [fontsize=8,fillcolor=%s,label=\"%s%sV=%.3f I=%.3f\"]" % (
            self.id(), self.__color,
            self.__label, separator,
            self.potential(), self.current())

    def printarcs(self):
        for node in self.__forw:
            # FIXME: the 0.009 heuristic needs refinement.  We may end
            # up removing the only arc.
            if node.potential() >= self.__potential + 0.0000009:
                print "\"%s\" -> \"%s\" [fontsize=9,label=\"I=%.3f\"]" % (
                    node.id(), self.id(),
                    (node.__weight * self.__weight * (
                    node.potential() - self.potential())))

    def visited(self):
        return self.__visited

    def prune_small_arcs(self):
        sum = 0
        max_cin = 0
        for node in self.__forw:
            cin = node.potential() - self.potential()
            if cin > 0:
                sum += cin
            if cin > max_cin:
                max_cin = cin

        ix = 0
        while ix < len(self.__forw):
            node = self.__forw[ix]
            cin = node.potential() - self.potential()
            if (cin < 0.2 * sum
                and cin < 0.5 * max_cin
                and node.prune_arc_from(self)):

                del self.__forw[ix]

            else:
                ix += 1

    def prune_arc_from(self, node_to_prune):
        sum = 0
        max_cout = 0
        for node in self.__back:
            cout = self.potential() - node.potential()
            if cout > 0:
                sum += cout
            if cout > max_cout:
                max_cout = cout

        try:
            ix = self.__back.index(node_to_prune)
        except ValueError:
            sys.stderr.write("cannot prune %s -> %s\n" % (
                node_to_prune.__uids[0],
                self.__uids[0]))
            return False
        cout = self.potential() - node_to_prune.potential()
        if cout < 0.2 * sum and cout < 0.5 * max_cout:
            del self.__back[ix]
            return True
        else:
            return False

    def merge_isomorphic_node(self):
        for forward in self.__forw:
            for sibling in forward.__back:
                if self.isomorphic(sibling):
                    self.merge(sibling)
                    return True
        return False

    def isomorphic(self, sibling):
        if self is sibling:
            return False
        if len(self.__forw) != len(sibling.__forw):
            return False
        if len(self.__back) != len(sibling.__back):
            return False
        for node in self.__forw:
            try:
                sibling.__forw.index(node)
            except ValueError:
                return False
        for node in self.__back:
            try:
                sibling.__back.index(node)
            except ValueError:
                return False
        return True

    def merge(self, sibling):
        for other in self.__forw:
            other.__back.index(sibling)
            other.__back.remove(self)
        for other in self.__back:
            other.__forw.index(sibling)
            other.__forw.remove(self)
        sibling.__weight += 1
        sibling.__label += "\\n\\n" + self.__label


class keyring:
    def __init__(self):
        self.__k = {}
        self.__k32 = {}

    def add(self, p):
        if self.__k.has_key(p):
            raise Badfile
        self.__k[p.id()] = p
        self.__k32.setdefault(p.id()[-8:], []).append(p.id())

    def prunesigs(self):
        for p in self.__k.values():
            p.prunesigs()

    def has_key(self, p):
        return self.__k.has_key(p)

    def node(self, p):
        return self.__k[p]

    def node32(self, p):
        res = self.__k.get(p)
        if res is not None:
            return res
        res = self.__k32[p]
        if len(res) != 1:
            raise ValueError("nonunique key %s expands to %s" % (
                k, ', '.join(res)))
        return self.__k[res[0]]

    def reset(self):
        for p in self.__k.values():
            p.reset()

    def mark_forbidden(self, k):
        self.node32(k).mark_forbidden()

    def find_mesh(self, trusted, target):
        tr = self.node32(trusted).id()
        ta = self.node32(target).id()

        self.__k[tr].mark()
        self.__k[ta].mark()
        pending = [(None, ta)]
        level = 1
        while not self.__k[tr].visited():
            next_pending = []
            #print "Level:", level, "pending:", len(pending)
            for (parent, node) in pending:
                vis = self.__k[node].visit(parent, level)
                next_pending = next_pending + vis
            if len(next_pending) == 0:
                break
            level = level + 1
            pending = next_pending

        if not self.__k[tr].visited():
            # No solution fond.
            return []

        pending = [(None, tr)]
        level = 0
        res = []
        while 1:
            next_pending = []
            #print "Rlevel:", level, "pending", len(pending)
            for (child, node) in pending:
                next_pending = next_pending + self.__k[node].rvisit(res, child)
            if len(next_pending) == 0:
                break
            level = level + 1
            pending = next_pending

        return res

    def init_potentials(self, nodes):
        for p in nodes.keys():
            self.node(p).init_potential(nodes)


def generate_graph(trusted, target, restrictions):
    global ring

    # sys.stdin.readline()
    # sys.stdin.readline()

    ring = k = keyring()
    p = None
    while 1:
        line = sys.stdin.readline()
        if line == '':
            break
        fields = line.split(":")
        if fields[0] == "pub":
            p = person(fields)
            k.add(p)
        elif fields[0] == "sig":
            p.set_sig(fields)
        elif fields[0] == "uid":
            p.set_uid(fields)
        elif fields[0] == "fpr":
            p.set_fpr(fields)
    k.prunesigs()
    g = graph(k, trusted, target)
    for r in restrictions:
        sys.stderr.write("Solved %s? %d\n" % (str(r), g.solve(r)))
    g.emit()

if __name__ == '__main__':
    trusted = "D294608E"
    target = sys.argv[1]
    restrictions = [[]]
    # restrictions = []
    for r in sys.argv[2:]:
        restrictions.append(r.split(","))
    generate_graph(trusted, target, restrictions)
